package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC003_EditLead extends Annotations {
	
	@BeforeClass
	public void setData() {
		excelFileName = "TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void editLeadTest(String cName, String fName, String lName) throws InterruptedException {
		new MyHomePage()
		.clickLeadsTab()
		.clickFindLeads()
		.typeFirstName(fName)
		.clickFindLeads(fName)
		.clickResultingLead()
		.clickEditbutton()
		.editCompanyName()
		.clickUpdateButton()
		.verifyCompanyName();		
	}
}