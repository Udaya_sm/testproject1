package tests;

//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC005_DuplicateLead extends Annotations {
	
	/*@BeforeClass
	public void setData() {
		excelFileName = "TC005";
	}*/
	
	@Test //(dataProvider="fetchData")
	public void duplicateLeadTest() throws InterruptedException {
		new MyHomePage()
		.clickLeadsTab()
		.clickFindLeads()
		.typeEmailAddress()
		.clickFindLeadsButton()
		.clickResultingLead()
		.clickDuplicateLead()
		.clickCreateLead();
	}
}