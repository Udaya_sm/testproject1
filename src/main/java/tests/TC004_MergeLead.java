package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC004_MergeLead extends Annotations {
	
	@BeforeClass
	public void setData() {
		excelFileName = "TC004";
	}
	
	@Test(dataProvider="fetchDataNum")
	public void mergeLeadsTest(int fLeadID, int tLeadID) throws InterruptedException {
		new MyHomePage()
		.clickLeadsTab()
		.clickMergeLeads()
		.typeFromLead(fLeadID)
		.typeToLead(tLeadID)
		.clickMergeButton()
		.acceptAlert()
		.clickFindLeads()
		.enterLeadID(fLeadID)
		.clickFindLeadsButton()
		.verifyUserMerged();
	}
}