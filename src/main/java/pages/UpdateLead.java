package pages;

import libraries.Annotations;

public class UpdateLead extends Annotations {
	
	public UpdateLead editCompanyName() {
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("newest");
		return this;
	}
	
	public ViewLeadPage clickUpdateButton() {
		driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();
		return new ViewLeadPage();
	}
}
