package pages;

import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals("udaya")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	
	public MyLeadsPage deleteLead() {
		driver.findElementByClassName("subMenuButtonDangerous").click();
		return new MyLeadsPage();
	}	
	
	public UpdateLead clickEditbutton() {
		driver.findElementByLinkText("Edit").click();
		return new UpdateLead(); 
	}
	
	public ViewLeadPage verifyCompanyName() {
		String cname2 = driver.findElementById("viewLead_companyName_sp").getText();
		String name1 = cname2.replaceAll("[^A-Za-z]", "");
		if(name1.equals("newest"))
			System.out.println("The company name is edited");
		else
			System.out.println("The company name is not changed"); 
		return this;
	}
	
	public FindLeadsPage clickFindLeads() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}
	
	public DuplicateLeadPage clickDuplicateLead() {
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeadPage();
	}
	public ViewLeadPage clickCreateLead() {
		driver.findElementByName("submitButton").click();
		return this;
	}
	
}
