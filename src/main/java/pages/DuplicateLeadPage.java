package pages;

import libraries.Annotations;

public class DuplicateLeadPage extends Annotations {
	
	public ViewLeadPage clickCreateLead() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}
	
}