package pages;

import libraries.Annotations;

public class FindLeadsPage extends Annotations {
	boolean displayed;
	
	public FindLeadsPage typeFirstName(String fName) {
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fName);
		return this;
	}
	
	public FindLeadsPage clickFindLeads(String fName) {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
	
	public ViewLeadPage clickResultingLead() throws InterruptedException
	{
		Thread.sleep(3000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a[1]").click();
		return new ViewLeadPage();
	}
	public FindLeadsPage enterLeadID(int fLeadID) {
		driver.findElementByName("id").sendKeys(String.valueOf(fLeadID));
		return this;
	}	
	public FindLeadsPage clickFindLeadsButton() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
	public FindLeadsPage verifyUserMerged() {
		displayed = driver.findElementByXPath("//div[text()='No records to display']").isDisplayed();
		System.out.println("User Account Merged: " + displayed);
		return this;
	}
	public FindLeadsPage typeEmailAddress() {
		driver.findElementByXPath("//span[text()='Email']").click();
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("check@check.com");
		return this;
	}
	
}