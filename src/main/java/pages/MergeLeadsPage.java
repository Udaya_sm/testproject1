package pages;

import org.openqa.selenium.Alert;

import libraries.Annotations;
// how to pass the numeric value to the string field

public class MergeLeadsPage extends Annotations {
	
	public MergeLeadsPage typeFromLead(int fLeadID) {
		driver.findElementById("ComboBox_partyIdFrom").sendKeys(String.valueOf(fLeadID));
		return this;
	}
	
	public MergeLeadsPage typeToLead(int tID) {
		driver.findElementById("ComboBox_partyIdTo").sendKeys(String.valueOf(tID));
		return this;
	}
	
	public MergeLeadsPage clickMergeButton()
	{
		driver.findElementByClassName("buttonDangerous").click();
		return this;
	}
	
	public ViewLeadPage acceptAlert() throws InterruptedException
	{
		Alert alert = driver.switchTo().alert();
		alert.accept();
		Thread.sleep(3000);
		return new ViewLeadPage();	
	}
}
